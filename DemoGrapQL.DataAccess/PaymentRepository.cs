﻿using DemoGrapQL.DataAccess.Repositories.Contracts;
using DemoGrapQL.Database;
using DemoGrapQL.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoGrapQL.DataAccess
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly DemoGrapQLContext _db;

        public PaymentRepository(DemoGrapQLContext db)
        {
            _db = db;
        }

        public IEnumerable<Payment> GetAllForProperty(int propertyId)
        {
            return _db.Payments.Where(x => x.PropertyId == propertyId);
        }
        public IEnumerable<Payment> GetAllForProperty(int propertyId, int lastAmount)
        {
            return _db.Payments.Where(x => x.PropertyId == propertyId)
                .OrderByDescending(x => x.DateCreated)
                .Take(lastAmount);
        }
    }
}
