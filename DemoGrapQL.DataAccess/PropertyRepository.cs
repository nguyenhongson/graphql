﻿using DemoGrapQL.DataAccess.Repositories.Contracts;
using DemoGrapQL.Database;
using DemoGrapQL.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoGrapQL.DataAccess
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly DemoGrapQLContext _db;

        public PropertyRepository(DemoGrapQLContext db)
        {
            _db = db;
        }

        public IEnumerable<Property> GetAll()
        {
            return _db.Properties;
        }

        public Property GetById(int id)
        {
            return _db.Properties.SingleOrDefault(x => x.Id == id);
        }

        public Property Add(Property property)
        {
            _db.Properties.Add(property);
            _db.SaveChanges();
            return property;
        }
    }
}
