﻿using DemoGraphQL.Mutations;
using DemoGraphQL.Queries;
using GraphQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoGraphQL.Schema
{
    public class DemoGraphQLSchema : GraphQL.Types.Schema
    {
        public DemoGraphQLSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<PropertyQuery>();
            Mutation = resolver.Resolve<PropertyMutation>();
        }
    }
}
