﻿using DemoGrapQL.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace DemoGrapQL.Database
{
    public class DemoGrapQLContext : DbContext
    {
        public DemoGrapQLContext(DbContextOptions<DemoGrapQLContext> options)
            : base(options)
        {

        }

        public DbSet<Property> Properties { get; set; }
        public DbSet<Payment> Payments { get; set; }
    }
}
